package br.com.springkafka.message;

import br.com.springkafka.config.MessageStreams;
import br.com.teste.Leda;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

@Service
public class ProducerV1 {

    private final MessageStreams messageStreams;

    public ProducerV1(MessageStreams messageStreams) {
        this.messageStreams = messageStreams;
    }

    public void send() {

        Leda leda = Leda.newBuilder()
                .setNumber1(24)
                .setNumber2(1f)
                .build();

        MessageChannel messageChannel = messageStreams.output();
        messageChannel.send(MessageBuilder
                .withPayload(leda)
                .build());



    }


}
