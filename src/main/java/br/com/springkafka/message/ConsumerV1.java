package br.com.springkafka.message;

import br.com.springkafka.config.MessageStreams;
import br.com.teste.Leda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.schema.client.ConfluentSchemaRegistryClient;
import org.springframework.cloud.stream.schema.client.SchemaRegistryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Service
public class ConsumerV1 {

    @Autowired
    ProducerV1 producerV1;

    @StreamListener(MessageStreams.INPUT)
    public void consumindoMensagens(Leda leda) {
        System.out.println("CONSUMI: ");
        System.out.println(leda.toString());
        System.out.println("tentando");

       producerV1.send();
    }

    @Configuration
    public class KafkaConfiguration {

        private String endPoint = "http://127.0.0.1:8081";

        @Bean
        public SchemaRegistryClient schemaRegistryClient() {
            ConfluentSchemaRegistryClient client = new ConfluentSchemaRegistryClient();
            client.setEndpoint(endPoint);
            return client;
        }
    }
}
